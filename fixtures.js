const mongoose = require('mongoose');
const config = require("./config");
const Cocktail = require('./models/Cocktail');
const User = require('./models/User');

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ['cocktails', 'users'];

db.once("open", async () => {

  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const admin = await User.create({
    facebookId: '1542389359217808',
    role: 'admin',
    displayName: 'Admin Admin',
    displayImage: 'brain-avatar.jpg'
  });
   await Cocktail.create({
     user: admin._id,
     title: 'Pinch Club',
     image: '1.jpg',
     recipe: 'Черносмородиновый джин легко сделать самому: 500 гр чёрной смородины залейте 1 л джина, оставьте настаиваться 2 суток, процедите — и готово. Коктейль тоже готовится просто: нужно влить все ингредиенты (кроме просекко) в шейкер и взболтать. Процедить в бокал «рокс» на лёд. Добавить пару капель эфирного масла лайма и долить просекко.',
     ingredients: [{name: 'черносмородинового джина', count: '45 мл'}, {name: 'сухого апельсинового ликёра', count: '20 мл'},
       {name: 'сока лайма', count: '15 мл'}, {name: 'просекко', count: '15мл'}, {name: 'деша ароматик биттера', count: '2'}, {name: 'деша апельсинового биттера', count: '2'}],
     rate: [{userId: admin._id, rating: 8}]
   }, {
     user: admin._id,
     title: 'Batanga',
     image: '2.jpg',
     recipe: 'Смешать все ингредиенты в стакане типа «хайбол», добавить лёд и долить лимонадом с экстрактом померанца. Украсить коктейль ножом, лаймом и щербетом.',
     ingredients: [{name: 'текила', count: '40 мл'}, {name: 'лимонный сок', count: '25 мл'},
       {name: 'сахарный сироп', count: '10 мл'}, {name: 'сироп «Солёная карамель»', count: '10 мл'}],
     rate: [{userId: admin._id, rating: 8}]
   });



  db.close();
});