const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  image: {
    type: String
  },
  recipe: {
    type: String,
    required: true
  },
  isPublish: {
    type: Boolean,
    default: false
  },
  ingredients: [{
    name: {
      type: String,
      required: true
    },
    count: {
      type: String,
      required: true
    }
  }],
  rate: [{
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    rating: {
      type: Number,
      required: true
    }
  }]
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;