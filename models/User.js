const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config');


const Schema = mongoose.Schema;

const UserSchema = new Schema({
  role: {
    type: String,
    default: 'user',
    enum: ['admin', 'user']
  },
  facebookId: {
    type: String,
    validate: {
      validator: async function (value) {
        if (!this.isModified('facebookId')) return true;

        const id = await User.findOne({facebookId: value});
        if (id) throw new Error('wrong facebook id');
      },
      message: 'Facebook ID already exist. Wrong facebook ID!'
    }
  },
  displayName: String,
  displayImage: String
});

UserSchema.methods.generateToken = function() {
  return jwt.sign({id: this._id}, config.jwt.secret, {expiresIn: config.jwt.expiresIn});
};

const User = mongoose.model('User', UserSchema);

module.exports = User;