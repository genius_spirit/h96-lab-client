const express = require("express");
const auth = require('../middleware/auth');
const config = require("../config");
const multer = require("multer");
const nanoid = require("nanoid");

const Cocktail = require("../models/Cocktail");
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const createRouter = () => {
const router = express.Router();

  router.get("/", async(req, res) => {
    try {
      const cocktails = await Cocktail.find();
      if (cocktails) res.send(cocktails);
    } catch (e) {
      return res.status(500).send({error: e})
    }
  });

  router.post("/", [auth, upload.single("image")], async(req, res) => {

    const data = req.body;

    if (req.file) {
      data.image = req.file.filename;
    } else {
      data.image = null;    }

    const cocktail = new Cocktail(data);

    try {
      const response = await cocktail.save();
      if (response) res.send(response);
    } catch (e) {
      return res.status(400).send({error: e});
    }
  });

  router.get('/about/:id', auth, async(req, res) => {
    const id = req.params.id;
    try {
      const cocktail = await Cocktail.findOne({_id: id});
      if (cocktail) res.send(cocktail);
      else res.status(404).send({message: 'Cocktail not found'})
    } catch (e) {
      return  res.status(400).send({error: e});
    }
  });

  router.post('/rating', auth, async(req, res) => {
    const id = req.body.id;
    const userId = req.body.userId;
    const objRate = req.body;

    // const cocktail = await Cocktail.findById({_id: id}, 'rate');
    //
    // cocktail.rate.forEach(item => {
    //     console.log(":________", item);
    //     if (userId === item.userId) console.log(1);
    // });

    const cocktail = await Cocktail.find({userId: {$in: [{userId}]}});
    if(cocktail) {
    console.log(1);
    } else {
      console.log('-1');
    }

    // try {
    //   const addRate = await Cocktail.findOneAndUpdate({_id: id}, {$push: {rate: objRate}});
    //   if (addRate) res.send({message: 'Your rating accepted'});
    // } catch (e) {
    //   res.status(400).send({message: e})
    // }
  });


  return router;
};

module.exports = createRouter;
