const express = require("express");
const nanoid = require('nanoid');
const User = require("../models/User");
const request = require("request-promise-native");
const config = require("../config");

const createRouter = () => {
const router = express.Router();

  router.post("/facebookLogin", async (req, res) => {
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`;

    try {
      const response = await request(debugTokenUrl);
      const decodedResponse = JSON.parse(response);

      if (decodedResponse.data.error) {
        return res.status(401).send({ message: "Facebook token incorrect" });
      }

      if (req.body.id !== decodedResponse.data.user_id) {
        return res.status(401).send({ message: "Wrong user ID" });
      }

      let user = await User.findOne({ facebookId: req.body.id });

      if (!user) {
        user = new User({
          facebookId: req.body.id,
          displayName: req.body.name,
          displayImage: req.body.picture.data.url
        });
        await user.save();
      }

      let token = user.generateToken();

      return res.send({ message: "User and password correct!", user, token });

    } catch (e) {
      return res.status(401).send({ message: "Facebook token incorrect" });
    }
  });

  router.delete("/sessions", async (req, res) => {
    const token = req.get("Token");
    const success = { message: "Logout success!" };
    if (!token) return res.send(success);

    const user = await User.findOne({ token });
    if (!user) return res.send(success);

    user.generateToken();
    await user.save();

    return res.send(success);
  });

  return router;
};

module.exports = createRouter;
