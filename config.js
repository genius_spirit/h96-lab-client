const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'cocktailRecipes'
  },
  jwt: {
    secret: 'secret-key',
    expiresIn: '1h'
  },
  facebook: {
    appId: "198626707426417",
    appSecret: "038ee732b6ec39228aa39c7cd6b3d947"
  }
};
